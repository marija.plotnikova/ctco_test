import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import junit.framework.Assert;

public class VerifyVacancyTest {

    public static void main (String[] args) throws Exception {

        System.setProperty("webdriver.chrome.driver","drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        String homeUrl = "http://ctco.lv";

        //open homepage
        driver.get(homeUrl);

        WebElement careers = driver.findElement(By.id("menu-item-127"));

        //hover over Careers in menu
        Actions action = new Actions(driver);
        action.moveToElement(careers).build().perform();

        //select Vacancies
        driver.findElement(By.id("menu-item-131")).click();

        //open Test Automation Engineer vacancy
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("menu-item-3249")));
        driver.findElement(By.id("menu-item-3249")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='text-block']/h1[.='Test Automation Engineer']")));

        //verify skills amount
        int len = driver.findElements(By.xpath("//div[contains(@class, 'vacancies-second-contents') and contains(@class ,'active')]//ul[1]//li")).size();

        try {
            Assert.assertEquals(len, 5);
        } catch (Exception e) {
            System.out.println("Error: skills amount is not equal to 5");
        }

        //close browser
        driver.close();
    }
}
